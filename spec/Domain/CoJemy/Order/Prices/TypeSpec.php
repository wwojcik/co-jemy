<?php

namespace spec\Domain\CoJemy\Order\Prices;

use Domain\CoJemy\Order\Prices\Type;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TypeSpec extends ObjectBehavior
{
    function it_creates_price_per_package_type()
    {
        $this->beConstructedThrough('pricePerPackage');
        
        $this->isEqualTo(Type::pricePerPackage())->shouldReturn(true);
    }

    function it_creates_delivery_cost_type()
    {
        $this->beConstructedThrough('deliveryCost');

        $this->isEqualTo(Type::deliveryCost())->shouldReturn(true);
    }
    
    function it_returns_false_when_is_different_than_other_type()
    {
        $this->beConstructedThrough('deliveryCost');

        $this->isEqualTo(Type::pricePerPackage())->shouldReturn(false);
    }

    function it_returns_true_when_is_the_same_as_other_type()
    {
        $this->beConstructedThrough('deliveryCost');

        $this->isEqualTo(Type::deliveryCost())->shouldReturn(true);
    }
}
