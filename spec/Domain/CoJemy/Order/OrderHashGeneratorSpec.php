<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Order\HashHolder;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class OrderHashGeneratorSpec extends ObjectBehavior
{
    function it_generates_new_hashes()
    {
        $hashHolder = $this->generate();
        $hashHolder->shouldHaveType(HashHolder::class);

        $hashHolder->getAdminHash()->shouldBeString();
        $hashHolder->getParticipantHash()->shouldBeString();
    }
}
