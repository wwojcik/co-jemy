#!/usr/bin/env bash

if [ "$BRANCH" == "master" ] && [ "$PULL_REQUEST" == false ]; then
    # Install heroku toolbelt
    which heroku || wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh
    heroku plugins:install heroku-docker

    # Add ssh key and set config
    test -f ~/.ssh/id_rsa.heroku || ssh-keygen -y -f /tmp/ssh/00_sub > ~/.ssh/id_rsa.heroku && heroku keys:add ~/.ssh/id_rsa.heroku
    heroku config:set PATH="/usr/local/bin:/usr/bin:/bin:/app/.heroku/php/bin:/app/.heroku/php/sbin" --app $APP_NAME

    # Perform release and migrations
    heroku docker:release --app $APP_NAME
    heroku run "cd /app/user ; composer migrate" --app $APP_NAME
else
    echo "Deployment process can be triggered only through merging to upstream master"
fi
