<?php

namespace Features\Application\Page\MenuItem;

use SensioLabs\Behat\PageObjectExtension\PageObject\Page;

class EditMenuItemPage extends Page
{
    protected $path = '/food-suppliers/{foodSupplierId}/menu-items/{menuItemId}/edit';

    protected $elements = [
        'Menu item form' => '#menu-item-form'
    ];

    /**
     * @param array $parameters
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function fillMenuItemForm(array $parameters)
    {
        $form = $this->getElement('Menu item form');
        $form->fillField('menu_item[name]', $parameters['name']);
        $form->fillField('menu_item[price]', $parameters['price']);

        $form->submit();
    }
}
