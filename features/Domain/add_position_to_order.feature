Feature: Add position to order
  As a user I want to...
  So that I...

  Scenario Outline:
    Given there are no orders in the system
    And there is order in the system with id "order123"
    When user <userId> adds position with <dishName>, <dishId> and <dishPrice> to order "order123" for <userNick>
    Then there should be "1" order with "1" position
    And the position should be <dishName>, <dishId>, <dishPrice> owned by <userNick>

    Examples:
      | dishName    | dishPrice | dishId | userId | userNick |
      | burger      | 19.00     | dish1  | user1  | nick1    |
      | frytki      | 5.00      | dish2  | user2  | nick1    |

  Scenario Outline:
    Given there are no orders in the system
    And there is order in the system with id "order123"
    When noid user adds position with <dishName>, <dishId> and <dishPrice> to order "order123" for <userNick>
    Then there should be "1" order with "1" position
    And the position should be <dishName>, <dishId>, <dishPrice> owned by <userNick>
    And the userId should be string

    Examples:
      | dishName    | dishPrice | dishId | userNick |
      | burger      | 19.00     | dish1  | nick1    |
      | frytki      | 5.00      | dish2  | nick1    |