Feature: Get suppliers

  Background:
    Given the database is empty

  Scenario: Get all suppliers
    Given there are food suppliers with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
      | supplier-2 | 20.00        | 60.00                 | 7.00              | 123-456-111 | http://supplier.gov | http://some-menu-url.co |
    When I send a GET request to "/api/suppliers"
    Then the response code should be 200
    And the response should contain json:
    """
    [
      {
        "id":1,
        "name":"supplier-1",
        "delivery_cost":10,
        "free_delivery_threshold":50,
        "single_package_cost":5,
        "phone_number":"123-456-789",
        "website_url":"http:\/\/supplier.com",
        "menu_url":"http:\/\/some-menu-url.pl",
        "menu_items":[]
      },
      {
        "id":2,
        "name":"supplier-2",
        "delivery_cost":20,
        "free_delivery_threshold":60,
        "single_package_cost":7,
        "phone_number":"123-456-111",
        "website_url":"http:\/\/supplier.gov",
        "menu_url":"http:\/\/some-menu-url.co",
        "menu_items":[]
      }
    ]
    """

  Scenario: Get supplier by id
    Given there are food suppliers with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
    And food supplier with id "1" has menu items with parameters:
      | name       | price        |
      | sushi      | 25           |
      | burger     | 12           |
    When I send a GET request to "/api/suppliers/1"
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "id":1,
      "name":"supplier-1",
      "delivery_cost":10,
      "free_delivery_threshold":50,
      "single_package_cost":5,
      "phone_number":"123-456-789",
      "website_url":"http:\/\/supplier.com",
      "menu_url":"http:\/\/some-menu-url.pl",
      "menu_items":
      [
        {
          "id":1,
          "name":"sushi",
          "price":25
        },
        {
          "id":2,
          "name":"burger",
          "price":12
        }
      ]
    }
    """

  Scenario: Get 404 when there are no suppliers
    When I send a GET request to "/api/suppliers"
    Then the response code should be 404
    And the response should contain json:
    """
    {
      "code":404,
      "message":"Suppliers not found."
    }
    """

  Scenario: Get 404 when there is no supplier with specified id
    When I send a GET request to "/api/suppliers/1"
    Then the response code should be 404
    And the response should contain json:
    """
    {
      "code":404,
      "message":"Supplier not found."
    }
    """
