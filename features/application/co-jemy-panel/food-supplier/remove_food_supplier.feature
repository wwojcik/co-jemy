@food-supplier
Feature: Removing food suppliers
  In order to keep food suppliers up to date
  As an administrator
  I have to be able to remove food supplier

  Scenario: Removing food supplier
    Given the database is empty
    And there is food supplier with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
    When I am on "/food-suppliers/1/delete"
    Then I should be on food suppliers list page
    And there should be no food suppliers
