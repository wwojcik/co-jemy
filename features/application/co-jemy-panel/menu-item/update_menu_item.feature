@menu-item
Feature: Update menu item
  In order to manage menu items
  As an administrator
  I have to be able to update menu

  Scenario: Update menu item
    Given the database is empty
    And there is food supplier with menu item with parameters:
      | name   | price  |
      | item-1 | 100.00 |
    When I am on "/food-suppliers/1/menu-items/1/edit"
    And I fill update menu item form with parameters:
      | name   | price |
      | item-2 | 300   |
    Then there should be menu item with parameters on food supplier page:
      | name   | price |
      | item-2 | 300   |
