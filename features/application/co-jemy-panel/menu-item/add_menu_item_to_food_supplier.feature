@menu-item
Feature: Add menu item to food supplier
  In order to define menu for supplier
  As an administrator
  I have to be able to add menu items

  Scenario: Add menu item to food supplier
    Given the database is empty
    And there is food supplier
    When I am on "/food-suppliers/1/menu-items/new"
    And I fill create menu item form with parameters:
      | name   | price |
      | item-1 | 100   |
    Then there should be menu item with parameters on food supplier page:
      | name   | price |
      | item-1 | 100   |
