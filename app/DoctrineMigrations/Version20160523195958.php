<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160523195958 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE food_suppliers_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE menu_items_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE food_suppliers (id INT NOT NULL, name VARCHAR(100) NOT NULL, delivery_cost NUMERIC(10, 2) NOT NULL, free_delivery_threshold NUMERIC(10, 2) NOT NULL, single_package_cost NUMERIC(10, 2) NOT NULL, phone_number VARCHAR(25) NOT NULL, website_url VARCHAR(100) NOT NULL, menu_url VARCHAR(500) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE menu_items (id INT NOT NULL, food_supplier_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, price NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_70B2CA2A8B6BBB42 ON menu_items (food_supplier_id)');
        $this->addSql('ALTER TABLE menu_items ADD CONSTRAINT FK_70B2CA2A8B6BBB42 FOREIGN KEY (food_supplier_id) REFERENCES food_suppliers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE menu_items DROP CONSTRAINT FK_70B2CA2A8B6BBB42');
        $this->addSql('DROP SEQUENCE food_suppliers_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE menu_items_id_seq CASCADE');
        $this->addSql('DROP TABLE food_suppliers');
        $this->addSql('DROP TABLE menu_items');
    }
}
