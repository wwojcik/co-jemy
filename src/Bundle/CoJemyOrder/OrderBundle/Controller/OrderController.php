<?php

namespace Bundle\CoJemyOrder\OrderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function indexAction() : Response
    {
        return $this->render('order\index.html.twig');
    }
}
