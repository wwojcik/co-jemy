<?php
/**
 * Created by PhpStorm.
 * User: mszoltysek
 * Date: 24.05.2016
 * Time: 11:35
 */

namespace Domain\CoJemy\Order;


class HashHolder
{
    private $adminHash;
    private $participantHash;

    /**
     * @return mixed
     */
    public function getAdminHash() : string
    {
        return $this->adminHash;
    }

    /**
     * @return mixed
     */
    public function getParticipantHash() : string
    {
        return $this->participantHash;
    }

    public function toArray() : array
    {
        return [
            'adminHash' => $this->getAdminHash(),
            'participantHash' => $this->getParticipantHash()
        ];
    }

    /**
     * HashHolder constructor.
     */
    private function __construct($adminHash, $participantHash)
    {
        $this->adminHash = $adminHash;
        $this->participantHash = $participantHash;
    }

    public static function createFromHashes($adminHash, $participantHash) : HashHolder
    {
        return new HashHolder($adminHash, $participantHash);
    }
}