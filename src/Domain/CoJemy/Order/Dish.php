<?php
/**
 * Created by PhpStorm.
 * User: mszoltysek
 * Date: 28.06.2016
 * Time: 14:33
 */

namespace Domain\CoJemy\Order;


use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\Prices\Type;

class Dish
{
    /** @var  string */
    private $name;

    /** @var  int */
    private $id;

    /** @var  Price */
    private $price;

    /**
     * Dish constructor.
     * @param string $name
     * @param int $id
     * @param Price $price
     */
    private function __construct($name, $id, $price)
    {
        $this->name = $name;
        $this->id = $id;
        $this->price = new Price(Type::dishCost(), $price);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $name
     * @param $id
     * @param $price
     * @return Dish
     */
    public static function withId($name, $id, $price)
    {
        return new Dish($name, $id, $price);
    }

    /**
     * @param $name
     * @param $price
     * @return Dish
     */
    public static function withoutId($name, $price)
    {
        return new Dish($name, null, $price);
    }
}