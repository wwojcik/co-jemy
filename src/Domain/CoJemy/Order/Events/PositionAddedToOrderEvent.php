<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class PositionAddedToOrderEvent implements Event
{
    private $aggregateId;
    private $userId;
    private $dishId;
    private $dishName;
    private $price;
    private $userNick;

    public function __construct(string $aggregateId, string $userId, string $dishId, string $dishName, float $price, string $userNick)
    {
        $this->aggregateId = $aggregateId;
        $this->userId = $userId;
        $this->dishId = $dishId;
        $this->dishName = $dishName;
        $this->price = $price;
        $this->userNick = $userNick;
    }

    public function getType() : string
    {
        return 'PositionAddedToOrderEvent';
    }

    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);
        $parameters->setParameter('userId', $this->userId);
        $parameters->setParameter('dishId', $this->dishId);
        $parameters->setParameter('dishName', $this->dishName);
        $parameters->setParameter('price', $this->price);
        $parameters->setParameter('userNick', $this->userNick);

        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return PositionAddedToOrderEvent
     */
    public static function fromParameters(array $parameters) : PositionAddedToOrderEvent
    {
        return new self(
            $parameters['aggregateId'],
            $parameters['userId'],
            $parameters['dishId'],
            $parameters['dishName'],
            $parameters['price'],
            $parameters['userNick']
        );
    }
}
