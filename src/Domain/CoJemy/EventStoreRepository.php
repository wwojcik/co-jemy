<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;

interface EventStoreRepository
{
    public function findOrderById(AggregateId $id) : Order;
}